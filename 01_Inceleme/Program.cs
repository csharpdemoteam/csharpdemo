﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace _01_Inceleme
{
    public class Program
    {
        public void testAbc()
        {

        }



        static void Main(string[] args)
        {
            //C:\Users\Cengiz\Desktop\Reflection_Inceleme\UdemyLib\UdemyLib\bin\Debug\UdemyLib.dll
            Assembly Lib = Assembly.LoadFile(@"Y:\devrepo-demo\ConsoleAppDemo\UdemyLib\bin\Debug\UdemyLib.dll");
            Type[] TP = Lib.GetTypes();
            foreach (Type item in TP)
            {
                ConstructorInfo[] CTORS = item.GetConstructors();
                for (int i = 0; i < CTORS.Length; i++)
                {
                    Console.WriteLine(CTORS[i].ToString());
                }

                PropertyInfo[] PRP = item.GetProperties();
                for (int i = 0; i < PRP.Length; i++)
                {
                    Console.WriteLine($"Namespace : {item.Namespace} Isim : {PRP[i].Name} Public : {item.IsPublic} Tam Adı : {item.FullName}");
                }

                Console.Clear();

                MethodInfo[] MTH = item.GetMethods();
                for (int i = 0; i < MTH.Length; i++)
                {
                    Console.WriteLine($"Metot Adı : {MTH[i].Name}");
                }
                Console.ReadLine();
            }
        }
    }
}
