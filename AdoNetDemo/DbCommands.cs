﻿using System;
using System.Data.SqlClient;

namespace AdoNetDemo
{
    public class DbCommands
    {
        public static void Main(string[] args)
        {
            DbCommands dbCommands=new DbCommands();

            dbCommands.createTable();


        }

        public void createTable()
        {
            ProductDal productDal = new ProductDal();
            productDal.ConnectionControl();

            SqlCommand command = new SqlCommand(
                @"CREATE TABLE dbo.Products2  
                    (ProductID int PRIMARY KEY NOT NULL,
                ProductName varchar(25) NOT NULL,
                Price money NULL,
                ProductDescription text NULL)", productDal.Connection);

            //command.Parameters.AddWithValue("name", product.Name); // @ olmadan da kabul ediyor
            //command.Parameters.AddWithValue("@unitPrice", product.UnitPrice);
            //command.Parameters.AddWithValue("@stockAmount", product.StockAmount);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //throw;
            }
            

            productDal.Connection.Close();

        }


    }
}