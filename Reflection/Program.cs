﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            //DortIslem dortIslem=new DortIslem(2,3);
            //Console.WriteLine(dortIslem.Topla2());
            //Console.WriteLine(dortIslem.Topla(3, 4));

            var tip = typeof(DortIslem);

            //DortIslem dortIslem = (DortIslem)Activator.CreateInstance(tip,6,7);
            //Console.WriteLine(dortIslem.Topla(4, 5));
            //Console.WriteLine(dortIslem.Topla2());

            // instance / obje oluşturma
            var instance = Activator.CreateInstance(tip, 6, 5);

            MethodInfo methodInfo =  instance.GetType().GetMethod("Topla2");
                
            Console.WriteLine(methodInfo.Invoke(instance,null));

            Console.WriteLine("------------------");
            var metodlar = tip.GetMethods();

            //foreach (var info in metodlar)
            //{
            //    Console.WriteLine("Metod adı : {0}",info.Name);
            //    foreach (var parameterInfo in info.GetParameters())
            //    {
            //        Console.WriteLine("Parametre : {0}",parameterInfo.Name);
            //    }

            //    foreach (var attribute in info.GetCustomAttributes())
            //    {
            //        Console.WriteLine("Attribute Name : {0}",attribute.GetType().Name);
            //    }
            //}

            var typeStudent = typeof(Student);

            //metodlar = typeStudent.GetMethods();

            //foreach (var info in metodlar)
            //{
            //    Console.WriteLine("Metod adı : {0}", info.Name);

            //    foreach (var parameterInfo in info.GetParameters())
            //    {
            //        Console.WriteLine("Parametre : {0}", parameterInfo.Name);
            //    }

            //    foreach (var attribute in info.GetCustomAttributes())
            //    {
            //        Console.WriteLine("Attribute Name : {0}", attribute.GetType().Name);
            //    }
            //}

            var propsStu = typeStudent.GetProperties();

            foreach (var propInfo in propsStu)
            {
                Console.WriteLine($"Prop Name :{propInfo.Name} Type {propInfo.PropertyType}");

                if (propInfo.PropertyType == typeof(string))
                {
                    Console.WriteLine("String Prop");
                }

                foreach (var attribute in propInfo.GetCustomAttributes())
                {
                    Console.WriteLine("Attribute Name : {0}", attribute.GetType().Name);

                    if (attribute.GetType() == typeof(MetodNameAttribute))
                    {
                        Console.WriteLine("Attr Metod Name Attribute Mevcut ");

                        var metodNameAttr = attribute as MetodNameAttribute;

                        Console.WriteLine(" Attr Metod Name:"+ metodNameAttr.name);

                    }

                }

                Console.WriteLine("");
            }


            Console.ReadLine();
        }
    }

    public class Student
    {
        [MetodName("Id")]
        public int id { get; set; }
        [MetodName("Name")]
        public string name { get; set; }
    }

    public class DortIslem
    {
        private int _sayi1;
        private int _sayi2;

        public DortIslem(int sayi1, int sayi2)
        {
            _sayi1 = sayi1;
            _sayi2 = sayi2;
        }

        public DortIslem()
        {
            
        }
        public int Topla(int sayi1, int sayi2)
        {
            return sayi1 + sayi2;
        }

        public int Carp(int sayi1, int sayi2)
        {
            return sayi1 * sayi2;
        }

        public int Topla2()
        {
            return _sayi1 + _sayi2;
        }

        [MetodName("Carpma")]
        public int Carp2()
        {
            return _sayi1 * _sayi2;
        }
    }

    public class MetodNameAttribute:Attribute
    {
        public string name { get; set; }

        public MetodNameAttribute(string name)
        {
            this.name = name;
        }
    }
}
